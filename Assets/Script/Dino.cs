﻿using UnityEngine;
using System.Collections;
using Apex.Steering;
using Apex.Steering.Components;

public class Dino : MonoBehaviour, IMoveUnits
{
	Transform _transform;
	Animator _animator;
	Rigidbody _rigidBody;

	int _speedId;
	int _angleId;

	//For Rotation
	Quaternion targetRotation;
	Quaternion orientation;

	Vector3 dinoForward;
	Vector3 targetForward;
	float dinoAngle;
	float targetAngle;
	float angleDifferent;

	void Awake()
	{
		_animator = GetComponent<Animator>();
		_rigidBody = GetComponent<Rigidbody>();

		_transform = transform;

		_speedId = Animator.StringToHash("DinoSpeed");
		_angleId = Animator.StringToHash("DinoAngle");

	}

	public void Move(Vector3 velocity, float deltaTime)
	{
		float speed = velocity.magnitude;
		_animator.SetFloat(_speedId, speed);
	
		if (!_rigidBody.isKinematic)
		{
			_rigidBody.velocity = _rigidBody.angularVelocity = Vector3.zero;
		}
		
		_rigidBody.MovePosition(_rigidBody.position + (velocity * deltaTime));
	}
	
	public void Rotate(Vector3 targetOrientation, float angularSpeed, float deltaTime)
	{

		targetRotation = Quaternion.LookRotation(targetOrientation);
		orientation = Quaternion.RotateTowards(_rigidBody.rotation, targetRotation, angularSpeed * Mathf.Rad2Deg * deltaTime);

		_rigidBody.MoveRotation(orientation);

		dinoForward = _rigidBody.rotation * Vector3.forward;
		targetForward = targetRotation * Vector3.forward;

		dinoAngle = Mathf.Atan2(dinoForward.x, dinoForward.z) * Mathf.Rad2Deg;
		targetAngle = Mathf.Atan2(targetForward.x, targetForward.z) * Mathf.Rad2Deg;

		angleDifferent = Mathf.DeltaAngle( dinoAngle, targetAngle );

		_animator.SetFloat(_angleId, angleDifferent);

	}
	
	public void Stop()
	{
		if (!_rigidBody.isKinematic)
		{
			_rigidBody.velocity = Vector3.zero;
			_rigidBody.Sleep();
		}

		_animator.SetFloat(_speedId, 0f);
		_animator.SetFloat(_angleId, 0f);
	}

}