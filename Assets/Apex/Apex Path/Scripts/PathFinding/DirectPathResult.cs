﻿/* Copyright © 2014 Apex Software. All rights reserved. */
namespace Apex.PathFinding
{
    using Apex.DataStructures;
    using Apex.WorldGeometry;
    using UnityEngine;

    /// <summary>
    /// The result of a path request that includes off grid navigation.
    /// </summary>
    public class DirectPathResult : PathResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DirectPathResult"/> class.
        /// </summary>
        /// <param name="from">Moving from.</param>
        /// <param name="to">Robing to.</param>
        /// <param name="originalRequest">The original request.</param>
        public DirectPathResult(Vector3 from, Vector3 to, IPathRequest originalRequest)
        {
            this.path = new Path(2);
            this.path.Push(new Position(to));
            this.path.Push(new Position(from));

            this.originalRequest = originalRequest;
            this.status = PathingStatus.Complete;
        }

        private DirectPathResult(PathingStatus status, IPathRequest originalRequest)
            : base(status, null, 0, originalRequest)
        {
        }

        /// <summary>
        /// Factory method to create a failure result
        /// </summary>
        /// <param name="status">The status.</param>
        /// <param name="originalRequest">The original request.</param>
        /// <returns>The result</returns>
        public static DirectPathResult Fail(PathingStatus status, IPathRequest originalRequest)
        {
            return new DirectPathResult(status, originalRequest);
        }
    }
}
