﻿/* Copyright © 2014 Apex Software. All rights reserved. */
namespace Apex.PathFinding
{
    using System.Collections.Generic;
    using Apex.WorldGeometry;
    using UnityEngine;

    /// <summary>
    /// Handles off grid navigation and other pre path finding tasks.
    /// </summary>
    public class DirectPather : IDirectPather
    {
        /// <summary>
        /// Resolves the direct path or delegates the request on to path finding.
        /// </summary>
        /// <param name="req">The request.</param>
        /// <returns>A path request to use in path finding or null if the path was resolved.</returns>
        public IPathRequest ResolveDirectPath(IPathRequest req)
        {
            var to = req.to;
            var from = req.from;
            var toGrid = req.toGrid;
            var fromGrid = req.fromGrid;

            if (fromGrid == null)
            {
                fromGrid = req.fromGrid = GridManager.instance.GetGrid(from);
            }

            if (toGrid == null)
            {
                toGrid = req.toGrid = GridManager.instance.GetGrid(to);

                //Just treat the to grid as the from grid in this case so the destination can be closest point on from grid
                if (toGrid == null)
                {
                    toGrid = req.toGrid = fromGrid;
                }
            }

            //If no grids were resolved for this request it means the request involves two points outside the grid(s) that do not cross any grid(s), so we can move directly between them
            if (fromGrid == null && toGrid == null)
            {
                req.Complete(new DirectPathResult(from, to, req));
                return null;
            }
            else if (fromGrid == null)
            {
                req.Complete(DirectPathResult.Fail(PathingStatus.StartOutsideGrid, req));
                return null;
            }

            return req;
        }
    }
}
