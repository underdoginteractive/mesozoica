﻿#pragma warning disable 1591
namespace Apex.Examples.SceneSpecific.PathRequests
{
    using System;
    using Apex.PathFinding;
    using Apex.Services;
    using UnityEngine;

    [AddComponentMenu("Apex/Examples/SceneSpecific/PathRequests/Path Request Example 2")]
    public class PathRequestExample2 : MonoBehaviour
    {
        public Transform target;

        private void Start()
        {
            var unit = this.GetUnitFacade();

            Action<PathResult> callback = (result) =>
            {
                if (result.status != PathingStatus.Complete)
                {
                    return;
                }

                if (result.pathCost > 200)
                {
                    Debug.Log("Too far away!");
                    return;
                }

                unit.MoveAlong(result.path);
            };

            var req = unit.CreatePathRequest(this.target.position, callback);

            GameServices.pathService.QueueRequest(req);
        }
    }
}
