﻿#pragma warning disable 1591
namespace Apex.Examples.SceneSpecific.PathRequests
{
    using System;
    using Apex.PathFinding;
    using Apex.Services;
    using UnityEngine;

    [AddComponentMenu("Apex/Examples/SceneSpecific/PathRequests/Path Request Example 3")]
    public class PathRequestExample3 : MonoBehaviour
    {
        public Transform target;

        private void Start()
        {
            var unit = this.GetUnitFacade();

            Action<PathResult> callback = (result) =>
            {
                if (result.status != PathingStatus.Complete)
                {
                    return;
                }

                Debug.Log("Cost of path was: " + result.pathCost);
            };

            var req = unit.CreatePathRequest(this.target.position, callback);

            req.type = RequestType.IntelOnly;

            GameServices.pathService.QueueRequest(req);
        }
    }
}
